package com.techelevator.model;

public class Leaderboard {
	
	private String userName;
	
	private int totalReading;
	
	private String month;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getTotalReading() {
		return totalReading;
	}

	public void setTotalReading(int totalReading) {
		this.totalReading = totalReading;
	}
	
	
	
	

}
